module.exports = function (state) {
  document.addEventListener('keydown', (e) => {
    var mag = 10
    switch (e.key) {
      case 'ArrowUp':
        state.usrPos.y -= mag
        state.usrCore.append(state.usrPos)
        break
      case 'ArrowDown':
        state.usrPos.y += mag
        state.usrCore.append(state.usrPos)
        break
      case 'ArrowRight':
        state.usrPos.x += mag
        state.usrCore.append(state.usrPos)
        break
      case 'ArrowLeft':
        state.usrPos.x -= mag
        state.usrCore.append(state.usrPos)
        break
    }
  })
}
