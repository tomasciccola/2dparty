module.exports = (state) => {
  var size = 10
  return function () {
    var canvas = document.querySelector('canvas')
    var ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    drawUsr(state.usrPos)
    state.usrs.forEach(usr => {
      drawUsr(usr.pos)
    })

    function drawUsr (pos) {
      ctx.strokeStyle = 'magenta'
      ctx.lineWidth = 2
      ctx.beginPath()
      ctx.arc(pos.x, pos.y, size, 0, 2 * Math.PI, false)
      ctx.stroke()
    }
  }
}
