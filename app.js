const choo = require('choo')
const html = require('choo/html')
const app = choo()
const devtools = require('choo-devtools')
const css = require('sheetify')

app.use(devtools())
app.use(require('./stores/masterCore.js'))
app.use(require('./stores/usrCore.js'))

var draw = require('./draw.js')
var keyboard = require('./keyboard.js')

app.use((state, emitter) => {
  state.loadedCore = false

  emitter.on('loadedCore', () => {
    setInterval(draw(state))
    keyboard(state)
  })
})

const style = css`
:host{
  background:#1e1e1e;
  color:white;
  font-family:monospace;
  font-size:24px;
  padding:20px;
  overflow:hidden;
}

:host canvas {
  width:100%;
  border: 2px solid white;
}
`
var onboard = require('./views/onboard.js')

app.route('/', (state, emit) => {
  return html`
    <body class=${style}>
    ${state.loadedCore ? main() : onboard(state, emit)}
    </body>
  `

  function main () {
    return html`
    <div>
      <header>${state.masterCore.key.toString('hex')}</header>
      <hr>
      <canvas></canvas>
    </div>
    `
  }
})

app.mount('body')
