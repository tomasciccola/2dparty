const html = require('choo/html')
const css = require('sheetify')

const onboardStyle = css`

:host{
  margin:0 auto;
  width:80%;
  border:2px solid white;
  padding:50px;
  margin-top:25%;
}

:host button#create {
  background:#1c1c1c;
  color:white;
  border:2px solid white;
  font-size:0.8em;
  padding:20px;
  width:20%;
  margin-left: 40%;
  margin-bottom:10px;
}

:host #join{
  display:flex;
  justify-content:center;
}

:host #join textarea{
  flex-grow:0.8;
  background: #1e1e1e;
  border:2px solid white;
  color:white;
  font-size:0.8em;

}

:host #join button{
  flex-grow:0.2;
  background: #1e1e1e;
  border:2px solid white;
  color:white;
  font-size:0.8em;
  padding:10px;

}
`

module.exports = (state, emit) => {
  return html`
        <div class=${onboardStyle}>
          <button id='create' onclick=${createOrJoin(true)}>crear</button>
          <div id='join'>
            <textarea id='inputKey'></textarea>
            <button onclick=${createOrJoin(false)}>unirse</button>
          </div>
        </div>
      `
  function createOrJoin (create) {
    return function (e) {
      var val = null
      var node = document.querySelector('#inputKey')
      if (!create) {
        val = node.value
        console.log(node.value)
        if (node.value.length === 0) return
      }
      emit('createOrJoin', val)
      node.val = ''
    }
  }
}
