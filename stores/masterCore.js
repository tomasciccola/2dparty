const SDK = require('dat-sdk')
const { Hypercore, destroy } = SDK()

module.exports = (state, emitter) => {
  emitter.on('createOrJoin', (key) => {
    // si key es null se crea uno nuevo
    state.masterCore = Hypercore(key, {
      extensions: ['discovery']
    })
    state.masterCore.on('ready', () => {
      // console.log(state.masterCore.key.toString('hex'))
      state.loadedCore = true
      emitter.emit('loadedCore')
      emitter.emit('render')
    })
  })
}
