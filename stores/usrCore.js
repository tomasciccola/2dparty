const SDK = require('dat-sdk')
const { Hypercore, destroy } = SDK()

module.exports = (state, emitter) => {
  state.usrs = []
  emitter.on('loadedCore', () => {
    state.usrCore = Hypercore(null, {
      valueEncoding: 'json',
      persist: false
    })
    state.usrCore.on('ready',
      () => {
        // Inicializá la posicion del usuario
        state.usrPos = { x: 0, y: 0 }
        state.usrCore.append(state.usrPos)
        // Escucha conexiones con peers y pasales tu key cuando te conectes
        state.masterCore.on('peer-add', (peer) => {
          console.log('Got a peer!')
          peer.extension('discovery', state.usrCore.key)
        })

        // cuando un peer te pasa su key empezá a seguirla!
        state.masterCore.on('extension', (type, message) => {
          console.log('Got extension message', type, message)
          if (type !== 'discovery') return
          // state.masterCore.close()
          // crea el hypercore del usr que apareció y mandalo al array
          const c = new Hypercore(message, {
            valueEncoding: 'json',
            persist: false
          })
          state.usrs.push(c)
          var usrIdx = state.usrs.indexOf(c)
          c.createReadStream({ live: true }).on('data', (d) => {
            console.log(d)
            state.usrs[usrIdx].pos = d
            emitter.emit('usrData')
          })
        })
      })
  })
}
